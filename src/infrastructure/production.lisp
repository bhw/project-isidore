;;;; SPDX-FileCopyrightText: 2021  Benedict H. Wang <foss@bhw.name>
;;;; SPDX-License-Identifier: AGPL-3.0-or-later
(in-package :cl-user)

(consfigurator:defpackage-consfig :project-isidore/infrastructure
  (:use #:common-lisp #:consfigurator)
  (:export #:oci-a1-flex))

(in-package :project-isidore/infrastructure)
(in-consfig "project-isidore/infrastructure")
(named-readtables:in-readtable :consfigurator)

(defparameter *prod-ql-dist-ver* "2023-10-21"
  "Production Quicklisp Distribution Version STRING passed as an ENV variable
'QL_DIST_VER', which is utilized in MAKE.LISP. Date must have the same format as
given in (ql:available-versions (ql::dist \"quicklisp\"))")

(defhost oci-a1-flex (:deploy ((:ssh :user "root") :sbcl))
  "Declarative Configuration for Production Server. To deploy to production run
locally in the listener,

CL-USER> (ql:quickload \"project-isidore/infrastructure\")
CL-USER> (project-isidore/infrastructure:oci-a1-flex)

NOTE that if (ql:quickload \"consfigurator\") fails with CFFI unable to find C
libraries, locally sudo apt install packages listed in
`consfigurator.property.package:+consfigurator-system-dependencies+'.

NOTE that if (oci-a1-flex) fails with an error on `consfigurator:connection-run'
add `project-isidore/infrastructure' :depends-on \"#:project-isidore\" and try
evaluating (oci-a1-flex) again. Hit [RETRY] on all prompts in the debugger.
Afterwards, remove \"#:project-isidore\" from :depends-on, restart the Lisp REPL
and evaluating (oci-a1-flex) now should work. Why is this? Heck if I know.

One piece of information is unique to this host: remote root SSH login access on
the local machine in ~/.ssh/config."
  (os:debian-stable "bookworm" :aarch64) ; Has SBCL version 2.2.9-debian.
  (timezone:configured "America/Toronto")
  (firewalld:zone-has-service "public" "https") ; Opens port 443.
  (firewalld:zone-has-service "public" "matrix") ; Opens port 8448.
  (cmd:single "rm -rf /usr/local/src/project-isidore/")
  (git:pulled "https://gitlab.com/bhw/project-isidore.git"
              (pathname "/usr/local/src/project-isidore/"))
  (cmd:single :env (list :BUILD_DIR "/usr/local/src/project-isidore/"
                         :QL_DIST_VER *prod-ql-dist-ver*)
              (concatenate 'string
                           "cd /usr/local/src/project-isidore/ && "
                           "sbcl "
                           ;; KLUDGE "--control-stack-size='4Mb' breaks."
                           "--control-stack-size '16Mb' "
                           "--dynamic-space-size '8Gb' "
                           ;; NOTE Use --script and not --load.
                           ;; http://www.sbcl.org/manual/#Toplevel-Options
                           "--script '/usr/local/src/project-isidore/make.lisp'"))
  ;; https://systemd-by-example.com/
  (file:has-content
   "/etc/systemd/system/project-isidore.service"
   "
[Unit]
Description=Project Isidore web application.
After=network.target

[Service]
Type=simple
User=root
Group=root
WorkingDirectory=/usr/local/src/project-isidore/
ExecStart=/usr/local/src/project-isidore/src/ProjectIsidore
Restart=always

[Install]
WantedBy=multi-user.target")
  (systemd:enabled "project-isidore.service")
  (systemd:restarted "project-isidore.service")
  ;; Reverse Proxy: Caddy will communicate with the Hunchentoot server(s)
  ;; locally. Forward requests from https://140.291.294.154:443 to
  ;; http://127.0.0.1:8080 where Hunchentoot accepts HTTP requests. Obviate need
  ;; to distribute CL+SSL (openssl) when providing release binaries.
  (apt:installed "caddy")
  (file:has-content
   "/etc/caddy/Caddyfile"
   "
bhw.name {
    redir https://www.{host}{uri}
}
www.bhw.name {
    reverse_proxy localhost:8080
}
matrix.bhw.name {
    reverse_proxy /_matrix/* localhost:8008
    reverse_proxy /_synapse/client/* localhost:8008
}
matrix.bhw.name:8448 {
    reverse_proxy /_matrix/* localhost:8008
}")
  (systemd:enabled "caddy")
  (systemd:restarted "caddy")
  (reboot:at-end))
