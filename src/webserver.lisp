;;;; SPDX-FileCopyrightText: 2021  Benedict H. Wang <foss@bhw.name>
;;;; SPDX-License-Identifier: AGPL-3.0-or-later

(uiop:define-package #:project-isidore/webserver
  (:use #:common-lisp #:series)
  (:mix-reexport #:snooze #:clog #:hunchentoot #:quux-hunchentoot)
  (:documentation
   "Added features to the Hunchentoot web server

1. URL routing library in Snooze.

2. Websocket protocol, RFC 6455, with Clog (transitive dependency on
websocket-driver).

3. A thread-pooling taskmaster for better performance with quux-hunchentoot.

Reexport symbols in these three packages under one unified namespace: webserver.

\"New features also include :mix and :reexport. :mix mixes imported symbols from
several packages: when multiple packages export symbols with the same name, the
conflict is automatically resolved in favor of the package named earliest,
whereas an error condition is raised when using the standard :use clause.
:reexport reexports the same symbols as imported from given packages, and/or
exports instead the same-named symbols that shadow them. ASDF 3.1 adds
:mix-reexport and :use-reexport, which combine :reexport with :mix or :use in a
single statement, which is more maintainable than repeating a list of packages.\"

-- http://fare.tunes.org/files/asdf3/asdf3-2014.html"))

(in-package #:project-isidore/webserver)

