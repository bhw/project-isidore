;;;; SPDX-FileCopyrightText: 2021  Benedict H. Wang <foss@bhw.name>
;;;; SPDX-License-Identifier: AGPL-3.0-or-later

(uiop:define-package #:project-isidore/test/tests
  (:use #:common-lisp #:series)
  (:import-from #:fxml/html5)
  (:local-nicknames
   (#:pi          #:project-isidore)
   (#:test        #:parachute)
   (#:http-client #:dexador)
   (#:xml         #:fxml))
  (:export #:master-suite)
  (:documentation
   "Project Isidore Regression Tests.

To run locally evaluate in the listener,

CL-USER> (ql:quickload \"project-isidore/test\")
CL-USER> (asdf:test-system \"project-isidore\")

The test suite is run prior to the build process. See MAKE.LISP."))

(in-package #:project-isidore/test/tests)

(test:define-test master-suite
  :description "The master suite of all Project Isidore tests")

;; (test:define-test can-app-init-and-exit
;;   :description "Check that application starts and exits gracefully. Terminate
;; application must be run otherwise `sb-ext:save-lisp-and-die' cannot save core as
;; there needs to be one thread, and initializing the application will create a
;; hunchentoot thread. We select port 3510 as port(s) 8080-8091 will be busy on the
;; production server."
;;   :parent master-suite
;;   (test:skip-on
;;       (win32) "Hunchentoot has poor Microsoft Windows support."
;;     (test:true (progn
;;                  (pi:initialize-application :port 3510)
;;                  (pi:terminate-application)))))


(test:define-test upgrade-dependency-p
  :description "Check if the Quicklisp and SBCL versions are the latest."
  :parent master-suite
  (test:finish
   (progn
     (if (< (reduce #'+ (mapcar #'parse-integer (ppcre:split "\\." (lisp-implementation-version))))
            (reduce #'+ (mapcar #'parse-integer (ppcre:split "\\." (car (ppcre:all-matches-as-strings
                                                                         "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}"
                                                                         (slot-value (vector-pop (slot-value (aref (fxml.dom:get-elements-by-tag-name (fxml.html:parse (dex:get "https://sbcl.org/news.html") (fxml-dom:make-dom-builder)) "h2") 0) 'fxml.rune-dom::children)) 'fxml.rune-dom::value)))))))
         (format t "~%=================================================
                    ~%Please upgrade the SBCL version to: ~a
                    ~%================================================="
                 (car (ppcre:all-matches-as-strings
                       "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}"
                       (slot-value (vector-pop (slot-value (aref (fxml.dom:get-elements-by-tag-name (fxml.html:parse (dex:get "https://sbcl.org/news.html") (fxml-dom:make-dom-builder)) "h2") 0) 'fxml.rune-dom::children)) 'fxml.rune-dom::value))))
         (format t "~%=================================================
                    ~%SBCL version is the latest: ~a
                    ~%================================================="
                 (lisp-implementation-version)))
     (if (local-time:timestamp<
          (local-time:parse-timestring (ql-dist:version
                                        (ql-dist:dist "quicklisp")))
          (local-time:parse-timestring (caar (ql-dist:available-versions
                                              (ql-dist:dist "quicklisp")))))
         (format t "~%==============================================================
                    ~%Please upgrade the Quicklisp distribution version to: ~a
                    ~%=============================================================="
                 (caar (ql-dist:available-versions (ql-dist:dist "quicklisp"))))
         (format t "~%==============================================================
                    ~%Quicklisp distribution version is the latest: ~a
                    ~%=============================================================="
                 (ql-dist:version (ql-dist:dist "quicklisp")))))))

(test:define-test does-global-css-exist
  :description "Global.css must exist in project-isidore/assets/global.css. The
  project generates CSS much in the same way it generates HTML. However unlike
  the generated HTML, CSS is not generated upon run time. The reason being
  complexity regarding how Common Lisp handles pathnames. Instead the function
  `generate-global-css' is manually called. This test ensures the static file
  global.css does indeed exist."
  :parent master-suite
  (test:true (uiop:file-exists-p (asdf:system-relative-pathname
                                :project-isidore "assets/global.css"))))

(test:define-test is-production-server-status-200
  :description "As per Heroku documentation: 'Whenever your app experiences an
  error, Heroku will return a standard error page with the HTTP status code
  503.'. This test serves as an early warning that the previous deploy may be
  successfully built, but there exists a runtime error.

  See Project Isidore documentation for more on the Drakma HTTP client library
  used.

  See https://httpstatuses.com/ for a list of HTTP status codes."
  :parent master-suite
  (test:true
   (= 200 (nth-value 1 (http-client:get "https://www.bhw.name/")))))

(test:define-test generate-data-finish
  :description "Check that `bible-page' and `bible-search-page' finishes."
  :parent master-suite
  (test:finish (progn
                 (pi:bible-page (list (pi:get-bible-uid 1 1 1)
                                      (pi:get-bible-uid 73 22 21)))
                 (pi:bible-search-page "water"))))

(test:define-test regex-validity
  :description "Check `*reference-regex*' still works with the version of
  CL-PPCRE in use."
  :parent master-suite
  (test:true (string-equal "1 Esdras 5:1" (car (ppcre:all-matches-as-strings pi:*reference-regex* (pi:get-footnotes-text (pi:get-bible-uid (pi:bible-book-convert-dwim "Zechariah") 1 1)))))))
